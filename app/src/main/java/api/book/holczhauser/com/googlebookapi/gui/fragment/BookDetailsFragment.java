package api.book.holczhauser.com.googlebookapi.gui.fragment;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Response;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;

import java.util.List;

import api.book.holczhauser.com.googlebookapi.R;
import api.book.holczhauser.com.googlebookapi.backend.dao.details.BookDetailResponse;
import api.book.holczhauser.com.googlebookapi.backend.dao.details.VolumeInfo;
import roboguice.fragment.RoboFragment;
import roboguice.inject.InjectView;

/**
 * Created by cholczhauser on 29/02/16.
 */
public class BookDetailsFragment extends RoboFragment implements Response.Listener<BookDetailResponse> {
    public static final String BOOKITEM_LINK = "ITEM_SELF_LINK";

    @InjectView(R.id.book_image)
    private ImageView iw_bookImage;

    @InjectView(R.id.book_title_value)
    private TextView tw_bookTitle;

    @InjectView(R.id.book_subtitle_value)
    private TextView tw_bookSubTitle;

    @InjectView(R.id.book_publisher_value)
    private TextView tw_bookPublisher;

    @InjectView(R.id.book_author_value)
    private TextView tw_bookAuthor;

    @InjectView(R.id.book_date_value)
    private TextView tw_bookDate;

    @InjectView(R.id.book_desc_value)
    private TextView tw_bookDescription;

    @InjectView(R.id.book_isbn_value)
    private TextView tw_bookISBN;

    @InjectView(R.id.progress_bar)
    private ProgressBar progressBar;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.book_details, null);
    }

    @Override
    public void onResponse(BookDetailResponse response) {

        assingValues(response);

    }

    /*Assign values to variables for displaying*/
    private void assingValues(BookDetailResponse response) {

        VolumeInfo itemVolumeInfo = response.getVolumeInfo();
        if (itemVolumeInfo.getTitle() != null) {
            tw_bookTitle.setText(itemVolumeInfo.getTitle().toString());
        } else {
            tw_bookTitle.setText(getResources().getString(R.string.no_data));
        }

        if (itemVolumeInfo.getAuthors() != null && itemVolumeInfo.getAuthors().size() > 0) {
            tw_bookAuthor.setText(createAuthorString(itemVolumeInfo.getAuthors()));
        } else {
            tw_bookAuthor.setText(getResources().getString(R.string.no_data));
        }

        if (itemVolumeInfo.getSubtitle() != null) {
            tw_bookSubTitle.setText(itemVolumeInfo.getSubtitle().toString());
        } else {
            tw_bookSubTitle.setText(getResources().getString(R.string.no_data));
        }

        if (itemVolumeInfo.getPublisher() != null) {
            tw_bookPublisher.setText(itemVolumeInfo.getPublisher().toString());
        } else {
            tw_bookPublisher.setText(getResources().getString(R.string.no_data));
        }

        if (itemVolumeInfo.getAuthors() != null && itemVolumeInfo.getAuthors().size() > 0) {
            tw_bookAuthor.setText(createAuthorString(itemVolumeInfo.getAuthors()));
        } else {
            tw_bookAuthor.setText(getResources().getString(R.string.no_data));
        }

        if (itemVolumeInfo.getPublishedDate() != null && itemVolumeInfo.getPublishedDate().length() > 1) {
            tw_bookDate.setText(itemVolumeInfo.getPublishedDate().toString());
        } else {
            tw_bookDate.setText(getResources().getString(R.string.no_data));
        }

        //let's assume a description more than 20 char long
        if (itemVolumeInfo.getDescription() != null && itemVolumeInfo.getDescription().length() > 20) {
            //removing HTML tags from text
            String desc = itemVolumeInfo.getDescription().toString();
            desc = desc.replaceAll("\\<.*?>", "");

            tw_bookDescription.setText(desc);
        } else {
            tw_bookDescription.setText(getResources().getString(R.string.no_desc));
        }

        if (itemVolumeInfo.getIndustryIdentifiers() != null && itemVolumeInfo.getIndustryIdentifiers().size() > 0) {
            tw_bookISBN.setText(response.getVolumeInfo().getIndustryIdentifiers().get(0).getIdentifier());
        } else {
            tw_bookISBN.setText(getResources().getString(R.string.no_data));
        }

        ImageLoader imageLoader = ImageLoader.getInstance();
        imageLoader.displayImage(itemVolumeInfo.getImageLinks().getSmall(), iw_bookImage, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {

            }
        });


    }


    private String createAuthorString(List<String> authors) {

        StringBuilder sb = new StringBuilder();
        sb.append("by:");
        for (String author : authors) {
            sb.append(author);
        }
        String authorNames = sb.toString();


        //creating ... if string exceesd device width
        int stringLimit = getResources().getInteger(R.integer.author_text_length);
        //creating a resource for supporting multiple layouts
        if (authorNames.length() > stringLimit) {
            authorNames = authorNames.substring(0, stringLimit);
            authorNames += "...";
        }

        return authorNames;
    }

}
