package api.book.holczhauser.com.googlebookapi.backend;

import android.content.Context;
import android.util.Log;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import java.util.HashMap;
import java.util.Map;

import api.book.holczhauser.com.googlebookapi.backend.dao.basic.BookResponse;
import api.book.holczhauser.com.googlebookapi.backend.dao.basic.GsonRequest;
import api.book.holczhauser.com.googlebookapi.backend.dao.details.BookDetailResponse;

/**
 * Created by cholczhauser on 25/02/16.
 */
@Singleton
public class BackendImpl implements IBackendInterface {

    private static final String backendURL = "https://www.googleapis.com/books/v1/volumes?maxResults=40&q=";

    private final RequestQueue queue;

    private final Response.ErrorListener errorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            if (error.getCause() instanceof ServerError) {
                Log.e("Volley ERROR", "ERROR DURING NETWORK COMMUNICATION");
            }
            if (error.getMessage() != null) {
                Log.e("Volley ERROR", error.getMessage());
            }
        }
    };


    @Inject
    public BackendImpl(Context context) {
        queue = Volley.newRequestQueue(context);
    }


    @Override
    public void getBookList(String query, Response.Listener successListener) {

        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");

        GsonRequest<BookResponse> myReq = new GsonRequest<BookResponse>(
                backendURL + query,
                BookResponse.class, headers,
                successListener,
                errorListener);

        queue.add(myReq);

    }

    @Override
    public void getBookDetails(String detailsURL, Response.Listener listener) {

        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");

        GsonRequest<BookDetailResponse> myReq = new GsonRequest<BookDetailResponse>(
                detailsURL,
                BookDetailResponse.class, headers,
                listener,
                errorListener);

        queue.add(myReq);
    }


}
