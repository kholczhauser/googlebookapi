package api.book.holczhauser.com.googlebookapi.gui;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.google.inject.Inject;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

import api.book.holczhauser.com.googlebookapi.R;
import api.book.holczhauser.com.googlebookapi.backend.AbstractBookResponse;
import api.book.holczhauser.com.googlebookapi.backend.dao.basic.BookResponse;
import api.book.holczhauser.com.googlebookapi.backend.dao.basic.Item;

/**
 * Created by cholczhauser on 26/02/16.
 */
public class BookListAdapter extends RecyclerView.Adapter<BookListAdapter.ViewHolder> {
    private Context applicationContext;
    private List<Item> itemList;

    private Response.Listener<AbstractBookResponse> successListener = new Response.Listener<AbstractBookResponse>() {
        @Override
        public void onResponse(AbstractBookResponse response) {
            if (response instanceof BookResponse) {
                List<Item> incomingList = ((BookResponse) response).getItems();
                itemList = incomingList;
                notifyDataSetChanged();
            }
        }


    };
    private BookActivity onClickListener;

    public Response.Listener<AbstractBookResponse> getSuccessListener() {
        return successListener;
    }


    @Inject
    public BookListAdapter() {
        if (itemList == null) {
            itemList = new ArrayList<>();
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, null);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setIndex(position);
        Item bookItem = itemList.get(position);

        holder.bookTitle.setText(bookItem.getVolumeInfo().getTitle());

        if (bookItem.getVolumeInfo().getAuthors().size() > 0) {
            holder.bookAuthor.setText(createAuthorString(bookItem));
        }

       /*
        imageLoader.displayImage(imageUri, holder.bookImage);
        */

    }

    private String createAuthorString(Item bookItem) {
        List<String> authors = bookItem.getVolumeInfo().getAuthors();
        StringBuilder sb = new StringBuilder();
        sb.append(applicationContext.getResources().getString(R.string.by));
        for (String author : authors) {
            sb.append(author);
        }
        String authorNames = sb.toString();


        //creating ... if string exceesd device width
        int stringLimit = applicationContext.getResources().getInteger(R.integer.author_text_length);
        //creating a resource for supporting multiple layouts
        if (authorNames.length() > stringLimit) {
            authorNames = authorNames.substring(0, stringLimit);
            authorNames += "...";
        }

        return authorNames;
    }


    @Override
    public int getItemCount() {
        if (itemList == null) {
            return 0;
        } else {
            return itemList.size();
        }
    }


    @Override
    public long getItemId(int i) {
        return i;
    }

    public void setOnClickListener(BookActivity onItemClickListener) {
        this.onClickListener = onItemClickListener;
    }


    public class ViewHolder extends RecyclerView.ViewHolder  implements View.OnClickListener{

        public ViewHolder(View v) {
            super(v);


            bookAuthor = (TextView) v.findViewById(R.id.book_author);
            bookTitle = (TextView) v.findViewById(R.id.book_title);
            context = v.getContext();

            if (applicationContext == null) {
                applicationContext = context.getApplicationContext();
            }
            v.setOnClickListener(this);
        }


        private TextView bookTitle, bookAuthor;
        private Context context;
        private int index;

        @Override
        public void onClick(View view) {
            onClickListener.onItemClicK(itemList.get(index));
        }

        public void setIndex(int index) {
            this.index = index;
        }
    }


}
