package api.book.holczhauser.com.googlebookapi.gui;

import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

import api.book.holczhauser.com.googlebookapi.R;
import api.book.holczhauser.com.googlebookapi.backend.BackendImpl;
import api.book.holczhauser.com.googlebookapi.backend.dao.basic.Item;
import api.book.holczhauser.com.googlebookapi.gui.fragment.BookDetailsFragment;
import api.book.holczhauser.com.googlebookapi.gui.fragment.BookRecyclerViewFragment;
import roboguice.RoboGuice;
import roboguice.activity.RoboActionBarActivity;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

@ContentView(R.layout.activity_book_list)
public class BookActivity extends RoboActionBarActivity implements BookItemClickListener {

    @InjectView(R.id.toolbar)
    Toolbar toolbar;

    @InjectView(R.id.toolbar_layout)
    CollapsingToolbarLayout toolbar_layout;
    private BookRecyclerViewFragment firstFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        RoboGuice.getInjector(this).injectViewMembers(this);
        configImageLoader();

        setSupportActionBar(toolbar);
        toolbar_layout.setTitle(getResources().getString(R.string.book_list));

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, getResources().getString(R.string.thanks), Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        // Create an instance of ExampleFragment
        firstFragment = new BookRecyclerViewFragment();
        // Add the fragment to the 'fragment_container' FrameLayout
        getSupportFragmentManager().beginTransaction()
                .add(R.id.fragment_root_element, firstFragment).commit();
    }

    private void configImageLoader() {
        DisplayImageOptions defaultOptions =
                new DisplayImageOptions.Builder()
                        .cacheInMemory()
                        .cacheOnDisc()
                        .showImageForEmptyUri(R.drawable.placeholder_icon)
                        .showStubImage(R.drawable.placeholder_icon)
                        .displayer(new FadeInBitmapDisplayer(500))
                        .build();

        ImageLoaderConfiguration mImageLoaderConfig =
                new ImageLoaderConfiguration.Builder(getApplicationContext())
                        .defaultDisplayImageOptions(defaultOptions)
                        .writeDebugLogs()
                        .build();
        ImageLoader.getInstance().init(mImageLoaderConfig);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_book_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    /*
     * Staring details fragment with the selected bookItem
     * param: Item type, can't be null
     */
    @Override
    public void onItemClicK(Item bookItem) {
        if (bookItem == null) {
            throw new NullPointerException("Item bookitem param NULL at onItemClick");
        } else {

            toolbar_layout.setTitle(bookItem.getVolumeInfo().getTitle());

            BookDetailsFragment detailsFragment = new BookDetailsFragment();
            new BackendImpl(this).getBookDetails(bookItem.getSelfLink(), detailsFragment);

            getSupportFragmentManager().beginTransaction().addToBackStack("details")
                    .replace(R.id.fragment_root_element, detailsFragment).commit();
        }
    }

    @Override
    public void onBackPressed() {
        //If we are stepping back to the main screen, then set the headerbar title
        if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
            toolbar_layout.setTitle(getResources().getString(R.string.book_list));
            super.onBackPressed();

        }
    }
}
