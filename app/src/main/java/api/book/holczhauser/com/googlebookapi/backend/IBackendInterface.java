package api.book.holczhauser.com.googlebookapi.backend;

import com.android.volley.Response;

/**
 * Created by cholczhauser on 25/02/16.
 */
public interface IBackendInterface {

    void getBookList(String query, Response.Listener listener);

    void getBookDetails(String detailsURL, Response.Listener listener);

}
