package api.book.holczhauser.com.googlebookapi.backend.dao.basic;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Item {

@SerializedName("kind")
@Expose
private String kind;
@SerializedName("id")
@Expose
private String id;
@SerializedName("etag")
@Expose
private String etag;
@SerializedName("selfLink")
@Expose
private String selfLink;
@SerializedName("volumeInfo")
@Expose
private VolumeInfo volumeInfo;

/**
* 
* @return
* The kind
*/
public String getKind() {
return kind;
}

/**
* 
* @param kind
* The kind
*/
public void setKind(String kind) {
this.kind = kind;
}

/**
* 
* @return
* The id
*/
public String getId() {
return id;
}

/**
* 
* @param id
* The id
*/
public void setId(String id) {
this.id = id;
}

/**
* 
* @return
* The etag
*/
public String getEtag() {
return etag;
}

/**
* 
* @param etag
* The etag
*/
public void setEtag(String etag) {
this.etag = etag;
}

/**
* 
* @return
* The selfLink
*/
public String getSelfLink() {
return selfLink;
}

/**
* 
* @param selfLink
* The selfLink
*/
public void setSelfLink(String selfLink) {
this.selfLink = selfLink;
}

/**
* 
* @return
* The volumeInfo
*/
public VolumeInfo getVolumeInfo() {
return volumeInfo;
}

/**
* 
* @param volumeInfo
* The volumeInfo
*/
public void setVolumeInfo(VolumeInfo volumeInfo) {
this.volumeInfo = volumeInfo;
}

}